package de.oszimt.ls81.csv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dariush
 *
 */
public class CSVHandler {

	/**
	 * muss sich im aktuellen Ordner befinden!
	 */
	private String file;
	private String delimiter;
	private String line = "";

	/**
	 * Default constructor
	 */
	public CSVHandler() {
		this(";", "studentNameCSV.csv");
	}

	/**
	 * Standard constructor
	 * 
	 * @param delimiter, Trennzeichen
	 * @param file,      Datei zum Einlesen
	 */
	// Constructor 2
	public CSVHandler(String delimiter, String file) {
		super();
		this.delimiter = delimiter;
		this.file = file;
	}

	/**
	 * Liest alle Schüler aus der csv aus und gibt sie zurück
	 * 
	 * @return List mit Schülern
	 */
	public List<Schueler> getAll() {

		Schueler s = null;
		List<Schueler> students = new ArrayList<Schueler>();
		BufferedReader reader;
		int nameIndex = 0;
		int jokerIndex = 0;
		int blamiertIndex = 0;
		int fragenIndex = 0;
// 5.Aufgabe     str+unschalte+5 formatierte Quelltext
		try {
			reader = new BufferedReader(new FileReader("studentNameCSV.csv"));
			String ueberschriften = reader.readLine();
			String[] titellist = ueberschriften.split(";");
			for (int i = 0; i < titellist.length; i++) {
				if (titellist[i].equals("Name")) {
					nameIndex = i;
				}
				if (titellist[i].equals("joker")) {
					jokerIndex = i;
				}
				if (titellist[i].equals("blamiert")) {
					blamiertIndex = i;
				}
				if (titellist[i].equals("fragen")) {
					fragenIndex = i;
				}
			}
			while (true) {
				String currentLine = reader.readLine();
//		1.Aufgabe		  wir brechen hier die Wiederholung/die Schleife ab
				if (currentLine == null) {
					break;
				}
				String[] wordlist = currentLine.split(";");
//n�chste Spalte 
				s = new Schueler(wordlist[nameIndex], Integer.parseInt(wordlist[jokerIndex]),
						Integer.parseInt(wordlist[blamiertIndex]), Integer.parseInt(wordlist[fragenIndex]));
				students.add(s);

//					for (String x: wordlist) {
//						
//						System.out.print(x);
//						System.out.print("\t");
//					}	System.out.println();    // n�chste Zeile 
//					
//				 System.out.println(currentLine);
			}

			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		return students;
	}

	/**
	 * Gibt alle Schüler aus
	 * 
	 * @param students
	 */
	public void printAll(List<Schueler> students) {
		for (Schueler s : students) {
			System.out.println(s.getName());
//    	System.out.println(s);
		}
	}
}
